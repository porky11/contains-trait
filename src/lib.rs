use collection_trait::Collection;

use std::{
    collections::{BTreeSet, HashSet, LinkedList, VecDeque},
    hash::Hash,
    ops::Deref,
};

pub trait Contains: Collection
where
    Self::Item: PartialEq,
{
    fn contains(&self, value: &Self::Item) -> bool;
}

macro_rules! impl_contains {
    ($t:ident) => {
        impl<T: PartialEq> Contains for $t<T> {
            #[inline]
            fn contains(&self, value: &T) -> bool {
                self.deref().contains(value)
            }
        }
    };
    ($t:ident, $($rest:ident), +) => {
        impl_contains!($t);
        impl_contains!($($rest),+);
    };
}

impl_contains!(Vec, VecDeque, LinkedList);

impl<T: Eq + Hash> Contains for HashSet<T> {
    #[inline]
    fn contains(&self, value: &T) -> bool {
        self.contains(value)
    }
}

impl<T: Ord> Contains for BTreeSet<T> {
    #[inline]
    fn contains(&self, value: &T) -> bool {
        self.contains(value)
    }
}
